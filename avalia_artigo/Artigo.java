import java.util.ArrayList;


public class Artigo
{
    private ArrayList<Aluno> autores;
    private String titulo, resumo;
    private Professor professorAutor;
    private Orientador orientadorAutor;
    private double nota;
    
    public Artigo() { 
        this.autores = new ArrayList<>();
    }
    
    public Artigo(String titulo, String resumo, double nota) {
        this.titulo = titulo;
        this.resumo = resumo;
        this.nota = nota;
        this.autores = new ArrayList<>();
    }
    
    public void addAutores(Aluno autor) {
        this.autores.add(autor);   
    }
    
    public void removeAutores (Aluno autor) {
        this.autores.remove(autor);   
    }
    
    public String getTitulo() {
        return this.titulo;   
    }
    
    public void setTitulo(String titulo) {
        this.titulo = titulo;   
    }
    
    public String getResumo() {
        return this.resumo;   
    }
    
    public void setResumo(String resumo) {
        this.resumo = resumo;   
    }
    
    public ArrayList<Aluno> getAutores() {
        return this.autores;   
    }
    
    public double getNota() {
        return this.nota;   
    }
    
    public void setNota(double nota) {
        this.nota = nota;   
    }
    
    public Professor getProfessorAutor() {
        return this.professorAutor;   
    }
    
    public void setProfessorAutor(Professor p) {
        this.professorAutor = p;   
    }
    
    public Orientador getOrientadorAutor() {
        return this.orientadorAutor;   
    }
    
    public void setOrientadorAutor(Orientador o) {
        this.orientadorAutor = o;   
    }    
}
