import java.util.ArrayList;


public class Autor
{
    private ArrayList<Aluno> alunosAutores;
    private Professor professorAutor;
    private Orientador orientadorAutor;
    
    public Autor() {
        this.alunosAutores = new ArrayList<>();
    }
    
    public void addAlunoAutor(Aluno aluno) {
        this.alunosAutores.add(aluno); 
    }
    
    public void removeAluno(Aluno aluno) {
        this.alunosAutores.remove(aluno);   
    }
    
    public ArrayList<Aluno> getAlunosAutores() {
        return this.alunosAutores;   
    }
    
    public Professor getProfessorAutor() {
        return this.professorAutor;   
    }
    
    public void setProfessorAutor(Professor p) {
        this.professorAutor  = p;   
    }
    
    public Orientador getOrientadorAutor() {
        return this.orientadorAutor;   
    }
    
    public void setOrientadorAutor(Orientador orientador) {
        this.orientadorAutor = orientador;   
    }
}
