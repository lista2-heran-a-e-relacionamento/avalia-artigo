import java.util.Scanner;


public class Principal{
    static Comite comite = new Comite();
    
    public static void main(String[] args) {
        cadastraArtigo();
        cadastraAvliadores();
        imprimeResultadoTresMelhoresArtigos();
        
    }
    
    public static void imprimeResultadoTresMelhoresArtigos() {
       
        System.out.println("Os três melhores artigos são: " );
        
        for(int i = 0; i < 3; i++) {
           System.out.println((i+1) +"º "+ comite.ordenaListaPorMaiorNota(comite.getArtigos()).get(i).getTitulo());   
        }
        
        
    }
    
    public static void cadastraAvliadores() {
        Scanner le = new Scanner(System.in);
        
        System.out.println("Inoforme o nome do avaliador");
        Avaliador aval = new Avaliador(le.next());
            
        System.out.println("Inoforme sua nota para os artigos de 0 a 10");
        
        for(int i = 0; i < comite.getArtigos().size();i++) {
           System.out.println("Informa a nota para o artigo " + comite.getArtigos().get(i).getTitulo());
           comite.getArtigos().get(i).setNota(le.nextDouble());
        }
       
    }
    
    public static void cadastraArtigo() {
        Scanner le = new Scanner(System.in);
        
        while(true) {
            Artigo art = new Artigo();
            
            System.out.println("Informe o título do artigo");
            art.setTitulo(le.next());
            
            System.out.println("Informe o resumo do artigo");
            art.setResumo(le.next());

            comite.addArtigo(art);
            cadastroAutores(art);
            
            System.out.println("Deseja incluir mais artigos? (S)im/(Não) ");
            if(le.next().equalsIgnoreCase("N")) {
                break;   
            }
        }
        
    }
    
    public static void cadastroAutores(Artigo art) {
        Scanner le = new Scanner(System.in);
        
        
        Autor autores = new Autor();
        
        System.out.println("##Cadastro dos autores do artigo##");
        
        System.out.println("##Informe o nome do orientador##");
        Orientador oriente = new Orientador(le.next());
        autores.setOrientadorAutor(oriente);
        art.setOrientadorAutor(oriente);
        
        System.out.println("##Informe o nome do professor##");
        Professor prof = new Professor(le.next());
        autores.setProfessorAutor(prof);
        art.setProfessorAutor(prof);
        
        Aluno aluno;
        while(true) {

            System.out.println("Informe o nome do aluno autor "); 
            aluno = new Aluno(le.next()); 
            
            autores.addAlunoAutor(aluno);
            art.addAutores(aluno);
             
            System.out.println("Deseja incluir mais aluno autor? (S)im/(Não) ");
            if(le.next().equalsIgnoreCase("N")) {
                break;   
            }
            
            
        }
        
    }
}
