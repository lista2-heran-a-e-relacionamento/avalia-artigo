import java.util.ArrayList;


public class Comite
{
     private ArrayList<Artigo> artigos;
     //private ArrayList<Artigo> artigos2;
     
    public Comite() {
        this.artigos = new ArrayList<>();
        //this.artigos2 = new ArrayList<>();
    }
     
    /*public void NotaArtigo() {
        double nota = 0;
        for(int i =0; i < getArtigos().size(); i++){
            nota = 0;
            for(int j =0; j < getAvaliador().size(); j++) {
                 nota += getAvaliador().get(j).getNotaAvaliador();   
            }
            
            getArtigos().get(i).setNota(nota);
        }
        
    }*/
    
    public ArrayList <Artigo> ordenaListaPorMaiorNota(ArrayList<Artigo> artigos ) {
    Artigo aux;
        
        for(int i = 0; i < artigos.size(); i++) {
            for(int j = 0; j < (artigos.size() - 1); j++) {
                if(artigos.get(j).getNota() < artigos.get(j + 1).getNota()) {
                    aux = artigos.get(j);
                    artigos.set(j,artigos.get(j + 1));
                    artigos.set(j + 1,aux);
                }
                
            }

        }
      return artigos;  
    }
    
    public void addArtigo(Artigo artigo) {
        this.artigos.add(artigo);
    }
    
    public void removeArtigo(Artigo artigo) {
        this.artigos.remove(artigo);   
    }
    
    /*public void addArtigo2 (Artigo ord) {
        this.avaliadores.add(avaliador);
    }
    
    public void removeAvaliador(Artigo ord) {
        this.avaliadores.remove(avaliador);   
    }*/
    
    public ArrayList <Artigo> getArtigos() {
        return this.artigos;   
    }
    
    
    /*public ArrayList <Artigo> getArtigo() {
        return this.artigo2;   
    }*/
}
